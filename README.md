# Task_11_Decompose_project



Analyze and decompose DjangoGramm. You should make some research and describe the project structure. 
Create UML class diagram for application and describe each module etc.

Storyline
Write a program like Instagram. This application must have a web interface. 
The user can register on the website by email. 
After basic registration, the user will receive a confirmation of the continuation of registration. 
The email must have a unique link. 
The user who goes by a link should be redirected to the profile page and add his full name, bio, and avatar. 
Next, the user can use DjangoGramm.
He can create posts with images, looks posts of other users via a feed of the latest posts. 
Unauthorized guests cannot view the profile and pictures of users.

Additional functional requirements (added 20.01.2022 by Vadym Serdiuk):
- Each post may have multiple images

- Each post may have multiple tags. New tags may be added by authors.

- Users may like posts (and unlike as well)

Треба спочатку придумати як її писати. 
З того що в задачі відомо треба намалювати 
- діаграму таблиць бази данних, 
- діаграму класів та 
- діаграму послідовності (sequence) для випадку реєстрація+логін

- для бази данних
https://dbdiagram.io/home
- Для всього іншого 
https://draw.io
